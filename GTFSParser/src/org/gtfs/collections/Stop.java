package org.gtfs.collections;

public class Stop {
	private long id;
	private String name;
	private double latitude;
	private double longitude;
	
	public Stop() {
	}
	
	public Stop(long id,
			String name,
			double lat,
			double lon) {
		this.id = id;
		this.name = name;
		this.latitude = lat;
		this.longitude = lon;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj != null) {
			return this.hashCode() == obj.hashCode();
		} else {
			return false;
		}
	}
}
