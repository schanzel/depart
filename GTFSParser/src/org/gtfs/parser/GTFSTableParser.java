package org.gtfs.parser;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.gtfs.collections.Stop;

public class GTFSTableParser {

	private Map<String, String[][]> tableData;
	public GTFSTableParser(Map<String, String[][]> tableData) {
		this.tableData = tableData;
	}
	
	public List<Stop> getStops() {
		List<Stop> retVal = new Vector<Stop>();
		
		String[][] stopData = tableData.get("stops.txt");
		
		String[] attrSeq = stopData[0];
		
		for(int i = 1; i < stopData.length; i++) {
			
			Stop stop = new Stop();
			
			for(int seq = 0; seq < attrSeq.length; seq++) {
				
				String currentAttr = attrSeq[seq].toUpperCase().replace("\"", "");
				
				if(currentAttr.equals("STOP_ID")) {
					stop.setId(Long.parseLong(stopData[i][seq]));
				} else if(currentAttr.equals("STOP_NAME")) {
					stop.setName((stopData[i][seq]).replace("\"", ""));
				} else if(currentAttr.equals("STOP_LON")) {
					stop.setLongitude(Double.parseDouble(stopData[i][seq]));
				} else if(currentAttr.equals("STOP_LAT")) {
					stop.setLatitude(Double.parseDouble(stopData[i][seq]));
				}
			}
			
			if(!retVal.contains(stop) && stop.getId() <= 9009999) {
				retVal.add(stop);
			}
		}
		
		return retVal;
	}
	
}
