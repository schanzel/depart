package org.gtfs.reader.impl;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

import org.gtfs.reader.GTFSReader;

public class GTFSFileReader implements GTFSReader {

	private static final Charset GTFS_STANDARD_CHARSET	= StandardCharsets.UTF_8;
	private static final String LINE_DELIMITER_PATTERN	= "\\r?\\n";
	private static final String FIELD_DELIMITER_PATTERN	= ",(?=([^\"]*\"[^\"]*\")*[^\"]*$)";
	
	private File[] files;
	public GTFSFileReader(File[] files) {
		this.files = files;
	}
	
	public Map<String, String[][]> getTableData() 
			throws Exception {
		
		Map<String, String[][]> retVal = new LinkedHashMap<String, String[][]>();
		
		try {
			for(File f : files) {
				String content = readFile(f, GTFS_STANDARD_CHARSET);
				String[] lines = content.split(LINE_DELIMITER_PATTERN);
				String[][] table = new String[lines.length][lines[0].split(FIELD_DELIMITER_PATTERN).length];
				
				for(int i = 0; i < lines.length; i++) {
					String[] values = lines[i].split(FIELD_DELIMITER_PATTERN);
					table[i] = values;
				}
				
				retVal.put(f.getName(), table);
			}
		} catch(Exception e) {
			System.err.println("Error parsing GTFS data from File");
			System.err.println(e);
			
			retVal = null;
		}
		
		return retVal;
	}
	
	private String readFile(File file, Charset encoding) throws IOException {
		byte[] bytes = Files.readAllBytes(Paths.get(file.getPath()));
		return encoding.decode(ByteBuffer.wrap(bytes)).toString();
	}
}
