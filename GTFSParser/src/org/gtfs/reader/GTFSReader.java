package org.gtfs.reader;

import java.util.Map;

public interface GTFSReader {
	Map<String,String[][]> getTableData() throws Exception;
}
