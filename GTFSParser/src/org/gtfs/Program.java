package org.gtfs;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.gtfs.collections.Stop;
import org.gtfs.parser.GTFSTableParser;
import org.gtfs.reader.GTFSReader;
import org.gtfs.reader.impl.GTFSFileReader;

public class Program {

	public static void main(String[] args) {
		File stopsFile = new File(Program.class.getResource("stops.txt").getPath());

		long start = System.currentTimeMillis();
		GTFSReader gtfsReader = new GTFSFileReader(new File[] { stopsFile });
		
		Map<String, String[][]> readerOutput = null;
		
		try {
			readerOutput = gtfsReader.getTableData();
		} catch (Exception e) {
			System.err.print(e);
		}

		GTFSTableParser parser = new GTFSTableParser(readerOutput);
		
		List<Stop> stops = parser.getStops();
		
		long end = System.currentTimeMillis();

		System.out.println("Parsing from files took " + (end - start) + " ms");
		System.out.println();
		
		for(Stop s : stops) {
			System.out.println(s.getName() + " - " + " Lat: " + s.getLatitude() + " Lon: " + s.getLongitude());
		}
	}

}
