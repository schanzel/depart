package org.depart.service.helper;

public class LocationHelper {
	
	public static final int EARTH_MEDIAN_RADIUS = 6371000;
	
	public static double distanceBetween(double lat1, double lon1, double lat2, double lon2)
	{   
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
        double d = EARTH_MEDIAN_RADIUS * c;
        
        return d;
	}
	
}
