package org.depart.service.collections;

import java.util.List;
import java.util.Vector;

import org.ding.api.collections.Departure;
import org.gtfs.collections.Stop;

public class Station {

	private String id;
	private String name;
	private double lat;
	private double lon;
	private double distance;
	private List<Departure> departures;
	
	public Station() {
		departures = new Vector<Departure>();
	}
	
	public Station(Stop stop) {
		this();
		this.id = Long.toString(stop.getId());
		name = stop.getName();
		lat = stop.getLatitude();
		lon = stop.getLongitude();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public List<Departure> getDepartures() {
		return departures;
	}

	public void setDepartures(List<Departure> departures) {
		this.departures = departures;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
