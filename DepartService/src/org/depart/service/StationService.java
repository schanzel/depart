package org.depart.service;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.depart.service.collections.Station;
import org.depart.service.helper.LocationHelper;
import org.ding.api.collections.Departure;
import org.ding.api.connector.Connector;
import org.gtfs.Program;
import org.gtfs.collections.Stop;
import org.gtfs.parser.GTFSTableParser;
import org.gtfs.reader.GTFSReader;
import org.gtfs.reader.impl.GTFSFileReader;

import com.google.gson.Gson;

@Path("/station")
public class StationService {
	
	private List<Stop> stops = null;
	private Connector dingApiConnector = null;
	
	private Gson jsonMarshaller = null;
	
	public StationService() {
		jsonMarshaller = new Gson();
		
		try {
			File stopsFile = new File(Program.class.getResource("/org/depart/resources/stops.txt").getPath());
			GTFSReader gtfsReader = new GTFSFileReader(new File[] { stopsFile });
			GTFSTableParser gtfsParser = new GTFSTableParser(gtfsReader.getTableData());
			stops = gtfsParser.getStops();
			
		} catch(Exception e) {
			System.err.println("Error parsing GTFS data! Cannot provide any service.");
		}
		
		if(stops != null) {
			dingApiConnector = new Connector();
		}
		
	}
	
	/**
	 * Gets a single {@link Station} by its name
	 * @param stationName
	 * @return the {@link Station} object as json {@link java.lang.String}
	 */
	@GET
	@Path("{station_id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String getStation(
			@PathParam("station_id") final long stationId,
			@QueryParam("lat") double lat,
			@QueryParam("lon") double lon) {
		
		Station station = null;
		
		for(Stop s : stops) {
			if(s.getId() == stationId) {
				station = new Station(s);
				break;
			}
		}
		
		if(station != null) {
			
			station.setDistance(LocationHelper.distanceBetween(lat, lon, station.getLat(), station.getLon()));
			
			try {
				List<Departure> departures = dingApiConnector.getItdData(station.getId()).getDepartureMonitorRequest().getDepartures();
				station.setDepartures(departures);
			} catch(Exception e) {
				System.err.println("Error receiving departure live data! Cannot provide further information for station " + station.getName());
			}
			
		} else {
			System.err.println("Given station id was not found: " + stationId);
		}
		
		return jsonMarshaller.toJson(station);
	}
	
	/**
	 * Gets a list of stations which are in a radius around a given geolocation 
	 * 
	 * @param lat the users current latitude as query parameter
	 * @param lon the users current longitude as query parameter
	 * @param rad the desired radius in witch to search for stations as query parameter
	 * @return a {@link List} containing {@link Station} which are within the given {@code radius}
	 */
	@GET
	@Path("/departures")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllStops(@QueryParam("lat") double lat, @QueryParam("lon") double lon, @QueryParam("radius") double rad) {
		List<Station> result = new Vector<Station>();
		
		for(Stop s : stops) {
			double distance = LocationHelper.distanceBetween(lat, lon, s.getLatitude(), s.getLongitude());
			
			if(distance <= rad) {
				Station station = new Station(s);
				station.setDistance(distance);
				
				try {
					List<Departure> departures = dingApiConnector.getItdData(station.getId()).getDepartureMonitorRequest().getDepartures();
					station.setDepartures(departures);
				} catch(Exception e) {
					System.err.println("Error receiving departure live data! Cannot provide further information for station " + s.getName());
				}
				
				result.add(station);
			}
		}
		
		Collections.sort(result, new Comparator<Station>() {
			public int compare(Station o1, Station o2) {
				return Double.compare(o1.getDistance(), o2.getDistance());
			}
		});
		
		return jsonMarshaller.toJson(result);
	}
	
	@GET
	@Path("names")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllStationsNames() {
		List<String> names = new Vector<>();
		for(Stop s : stops) {
			names.add(s.getName());
		}
		return jsonMarshaller.toJson(names);
	}
	
}