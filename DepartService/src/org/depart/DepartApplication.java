package org.depart;

import org.glassfish.jersey.server.ResourceConfig;

public class DepartApplication extends ResourceConfig{
	
	private final String[] packages = {
			"org.depart.service"
	};
	
	public DepartApplication() {
		System.out.println("Initializing services");
		for(String p : packages) {
			System.out.println(p);
		}
		packages(packages);
	}
}
