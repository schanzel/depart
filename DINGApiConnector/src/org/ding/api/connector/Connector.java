package org.ding.api.connector;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.zip.GZIPInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.ding.api.collections.ItdRequest;

public class Connector {
	
	private static final String URL_TEMPLATE		= "http://www.ding.eu/ding2/XML_DM_REQUEST?laguage=de&typeInfo_dm=stopID&nameInfo_dm={stopName}&deleteAssignedStops_dm=1&useRealtime=1&mode=direct&excludedMeans=0";
	private static final String URL_ENCODING		= "UTF-8";
	
	private static final String HTTP_METHOD			= "GET";
	
	
	
	private URL url = null;
	private HttpURLConnection httpConnection = null;
	
	
	public Connector() {
		
	}
	
	public ItdRequest getItdData(String stopName) {
		
		ItdRequest retVal = null;
		
		
		try {
			stopName = URLEncoder.encode(stopName, URL_ENCODING);
			String targetUrl = URL_TEMPLATE.replace("{stopName}", stopName);
			
			url = new URL(targetUrl);
			
			httpConnection = (HttpURLConnection)url.openConnection();
			
			httpConnection.setRequestMethod(HTTP_METHOD);
			
			httpConnection.setRequestProperty("Accept", "application/xml");
			httpConnection.setRequestProperty("Accept-Encoding", "gzip,deflate,sdch");
			httpConnection.setRequestProperty("Accept-Language", "de,en-US;q=0.8,en;q=0.6");
			httpConnection.setRequestProperty("Cache-Control", "no-cache");
			httpConnection.setRequestProperty("Connection", "keep-alive");
			httpConnection.setRequestProperty("Host", "www.ding.eu");
			httpConnection.setRequestProperty("Pragma", "no-cache");
			httpConnection.setRequestProperty("DNT", "1");
			
			httpConnection.setReadTimeout(3000);
			
			httpConnection.setUseCaches(false);

			//get response
			httpConnection.getResponseMessage();
			InputStream input = httpConnection.getInputStream();

			GZIPInputStream gzipInputStream = new GZIPInputStream(input);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(gzipInputStream));
			
			String line;
			StringBuffer sb = new StringBuffer();
			
			while((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
			}
			
			reader.close();
			
			String xml = sb.toString();
			xml = URLDecoder.decode(xml, "UTF-8");
			retVal = unmarshalItdRequest(xml);
		
		} catch (Exception e) {
			System.err.println(e);
			
		} finally {
			httpConnection.disconnect();
		}
		return retVal;
	}
	
	private ItdRequest unmarshalItdRequest(String xmlInput) throws JAXBException {
		ItdRequest retVal = null;
		
		JAXBContext jaxbContext = JAXBContext.newInstance(ItdRequest.class);
		
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		StringReader sr = new StringReader(xmlInput);
		
		retVal = (ItdRequest)unmarshaller.unmarshal(sr);
		
		return retVal;
	}
	
}
