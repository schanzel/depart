package org.ding.api.collections;

import java.util.List;
import java.util.Vector;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="itdDepartureMonitorRequest")
public class DepartureMonitorRequest {
	
	private List<Departure> departures;
	
	public DepartureMonitorRequest() {
		departures = new Vector<Departure>();
	}
	
	public DepartureMonitorRequest(List<Departure> departures) {
		this.departures = departures;
	}

	public List<Departure> getDepartures() {
		return departures;
	}

	@XmlElement(name="itdDeparture")
	@XmlElementWrapper(name="itdDepartureList")
	public void setDepartures(List<Departure> departures) {
		this.departures = departures;
	}
	
	

}
