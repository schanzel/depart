package org.ding.api.collections;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="itdRequest")
public class ItdRequest {

	private DepartureMonitorRequest departureMonitorRequest;
	
	public ItdRequest() {
	}
	
	public ItdRequest(DepartureMonitorRequest departureMonitorRequest) {
		this.departureMonitorRequest = departureMonitorRequest;
	}

	public DepartureMonitorRequest getDepartureMonitorRequest() {
		return departureMonitorRequest;
	}
	
	@XmlElement(name="itdDepartureMonitorRequest")
	public void setDepartureMonitorRequest(DepartureMonitorRequest departureMonitorRequest) {
		this.departureMonitorRequest = departureMonitorRequest;
	}
	
	
}
