package org.ding.api.collections;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="itdDeparture")
public class Departure {
	
	private String stopName;
	
	private int countdown;
	
	private ServingLine servingLine;
	
	public Departure() {
	}
	
	public Departure(String stopName, int countdown, ServingLine servingLine) {
		this.stopName = stopName;
		this.countdown = countdown;
		this.servingLine = servingLine;
	}
	
	public String getStopName() {
		return stopName;
	}
	
	@XmlAttribute(name="stopName")
	public void setStopName(String stopName) {
		this.stopName = stopName;
	}
	
	public int getCountdown() {
		return countdown;
	}
	
	@XmlAttribute(name="countdown")
	public void setCountdown(int countdown) {
		this.countdown = countdown;
	}
	
	public ServingLine getServingLine() {
		return servingLine;
	}

	@XmlElement(name="itdServingLine")
	public void setServingLine(ServingLine servingLine) {
		this.servingLine = servingLine;
	}
}
