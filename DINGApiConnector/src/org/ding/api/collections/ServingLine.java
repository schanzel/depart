package org.ding.api.collections;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="itdServingLine")
public class ServingLine {
	
	private String direction;
	private String number;
	
	public ServingLine() {
	}
	
	public ServingLine(String direction, String number) {
		this.direction = direction;
		this.number = number;
	}

	public String getDestination() {
		return direction;
	}

	@XmlAttribute(name="direction")
	public void setDestination(String direction) {
		this.direction = direction;
	}

	public String getNumber() {
		return number;
	}
	
	@XmlAttribute(name="number")
	public void setNumber(String number) {
		this.number = number;
	}
	
	
}
