package org.ding.api;

import java.util.List;

import org.ding.api.collections.Departure;
import org.ding.api.collections.ItdRequest;
import org.ding.api.connector.Connector;

public class Program {

	public static void main(String[] args) {
		
		String stopName = "Eselsberg Hasenkopf"; 
		if(args.length > 0) {
			stopName = args[0];
		}
		
		Connector c = new Connector();
		
		ItdRequest itdData = c.getItdData(stopName);
		
		List<Departure> departures = itdData.getDepartureMonitorRequest().getDepartures();
		
		System.out.println("Abfahrtszeiten: " + stopName);
		System.out.println("-----------------------------------");
		for(Departure d : departures) {
			
			System.out.println(d.getServingLine().getNumber() + " - " +
					d.getServingLine().getDestination() + " : "
					+ d.getCountdown() + " min");
			
		}
	}

}
